//
//  Currency.swift
//  CurrencyExchangeExample
//
//  Created by PixelByte on 24/10/2017.
//  Copyright © 2017 PixelByte. All rights reserved.
//

import Foundation

//currency object
class Currency {
    
    var countryName: String
    var rates: Float
    
    public init(countryName: String, rates: Float) {
        self.countryName = countryName
        self.rates = rates
    }
}
